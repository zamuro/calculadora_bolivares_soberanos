set :stage, :production
set :application, "calculadora_bolivares_soberanos"
set :bundle_flags, '--quiet' 
set :bundle_bins, %w(rake rails)
set :bundle_path, nil

role :app, %w{leprechaun.elrancho.com.ve}
role :web, %w{leprechaun.elrancho.com.ve}

server "leprechaun.elrancho.com.ve", user: "manuel", roles: %w{app web}
