lock "~> 3.11.0"

set :application, "calculadora_bolivares_soberanos"
set :repo_url, "git@gitlab.com:zamuro/calculadora_bolivares_soberanos"
 set :deploy_to, "/var/www/html/calculadora.adoptaunvenezolano.org"
set :keep_releases, 5
set :ssh_options, verify_host_key: :always
