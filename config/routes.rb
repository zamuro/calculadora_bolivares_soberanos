Rails.application.routes.draw do
  get 'calculations/soberanos'
  get 'calculations/fuertes'

  root 'calculations#index'
end
