class CalculationsController < ApplicationController
  def index
  end

  def soberanos
    value = params[:soberanos][:value] if params[:soberanos].present?
    @result = Calculation.soberanos(value.to_f) if value.present?
  end

  def fuertes
    value = params[:fuertes][:value] if params[:fuertes].present?
    @result = Calculation.fuertes(value.to_f) if value.present?
  end
end
