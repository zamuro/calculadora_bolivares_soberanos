class Calculation
  include ActiveModel::Model

  def self.soberanos(value=nil)
    value / 100000 unless value.nil?
  end
  def self.fuertes(value=nil)
    value * 100000 unless value.nil?
  end
end
