require 'rails_helper'

RSpec.describe Calculation, type: :model do
  describe "calculations" do
    it "calculates bolivares soberanos" do
      value = 200000
      result = Calculation.soberanos(value)
      expect(result).to eq(2)
    end

    it "calculates bolivares fuertes" do
      value = 2
      result = Calculation.fuertes(value)
      expect(result).to eq(200000)
    end
  end
end
